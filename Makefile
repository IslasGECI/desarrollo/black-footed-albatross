.PHONY: down

INTERNAL_SERVER_REPOSITORY_DIRECTORY=/home/ciencia_datos/repositorios/black-footed-albatross
MYSQL_DATABASE_URL := https://gitlab.com/api/v4/projects/23173360/repository/files/databases%2Fgeci2012_blog_bfal_bd_parcial.sql/raw?ref=

.env:
	cp --update example.env ./.env

start: .env
	$(eval include .env)
	$(eval include ~/.vault/.secrets)
	sudo git submodule update --remote
	mkdir --parents WordPressThemes/plugins
	docker-compose up -d --build
	sudo chmod --recursive 777 ./wordpress

healthcheck:
	docker-compose run --rm healthcheck

down:
	docker-compose down

install: start healthcheck

configure:
	docker-compose --file docker-compose.yml --file wp-auto-config.yml run --rm wp-auto-config

autoinstall: start configure download_database download_multimedia_files final_message

clean: down
	@echo "💥 Removing related folders/files..."
	@rm --force --recursive mysql/* wordpress/* WordPressThemes/plugins/*
	docker volume rm -f geci2012_blog_bfal_bd_bfal_mysql
	rm --force .env

internal_deploy: internal_clean
	ssh ciencia_datos@islasgeci.org "cd $(INTERNAL_SERVER_REPOSITORY_DIRECTORY) && \
		git pull && \
		sed 's/localhost/islasgeci.org/g' example.env > .env && \
		source ~/.vault/.secrets && \
		make autoinstall"

internal_clean:
	ssh ciencia_datos@islasgeci.org "cd $(INTERNAL_SERVER_REPOSITORY_DIRECTORY) && \
		cp example.env .env && \
		sudo make clean && \
		rm --force .env"

download_database:
	mkdir --mode 755 --parents tmp
	curl --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "$(MYSQL_DATABASE_URL)${GITLAB_DATABASE_BRANCH}" --output tmp/data.sql 
	docker exec --interactive geci2012_blog_bfal_bd_mysql mysql --user=${DATABASE_USER} --password=${DATABASE_PASSWORD} geci2012_blog_bfal_bd < tmp/data.sql

download_multimedia_files:
	if [ ! -f ./tmp/uploads.zip ] ; then \
     	sudo wget --output-document=./tmp/uploads.zip "https://www.islas.org.mx/albatros-patas-negras/wp-content/uploads.zip"; \
	fi;
	sudo unzip -o ./tmp/uploads.zip -d wordpress/wp-content

final_message:
	@echo "¡La instalación fue un exito!"