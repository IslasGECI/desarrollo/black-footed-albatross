# Kit de despliegue de Blog de Albatros Patas Negras

Este kit para desplegar blog de BFAL incluye lo siguiente:

- Nginx web server
- Wordpress
- MariaDB/MySQL usado para la base de datos de Wordpress
- phpMyAdmin interfaz de MySQL
- WP-CLI: Wordpress Command Line Interface
- Makefile para automatización


# Requisitos:
  - [Docker 20.10.*](https://github.com/IslasGECI/misctools#-prerequisitos)
  - [Docker-Compose 1.29.*](https://docs.docker.com/compose/install/)
  - Tener configurada la variable de entorno __$PRIVATE_TOKEN__ tanto en nuestra
    computadora como en nuestro servidor islasgeci.org
  - Tener configurada la comunicación ssh con nuestro servidor islasgeci.org.

# Configurar la variable __$PRIVATE_TOKEN__
 Lo primero que debemos hacer antes de levantar una instalación es verificar que
 la siguiente variable de entorno en el __~/.vault/.secrets__ está disponible:

 ``` bash
 export PRIVATE_TOKEN=<VALOR DEL TOKEN>
 ``` 
 
 Esta variable de entorno es de suma importancia para el acceso y descarga de
 nuestros repositorios de GitLab. De no tenerla configurada, deberemos solicitar
 el _token_ al encargado actual del repositorio y agregarla a nuestro archivo
 __~/.vault/.secrets__.


 Después de configurar la variable, deberemos ejecutar el siguiente comando para
 cargar dicha variable en el sistema:
 ``` bash
 source ~/.vault/.secrets
 ```




# Instalación local - localhost:80
Para desplegar un entorno local del blog sigue las siguientes instrucciones:



## 1. Clona el repositorio
``` bash
git clone https://gitlab.com/IslasGECI/patas_negras/black-footed-albatross.git
cd black-footed-albatross
```

## 2. Configuración de la rama del tema
En este paso de configuración debemos especificar la rama del tema que deseamos
probar. Generalmente será alguna _feature_, aunque por defecto ya está
configurada como `develop`. Esto lo logramos asignando el nombre de dicha rama
en la variable: __GITLAB_DATABASE_BRANCH__ ubicada en el archivo `example.env`. Ejemplo:

``` bash
GITLAB_DATABASE_BRANCH=develop
```
Entonces configuramos la rama `feature`:
``` bash
GITLAB_DATABASE_BRANCH=feature/wp-archivos-principales
```

Una vez que se envien los cambios de una rama `feature` en la que estemos
trabajando alguna caracteristica del tema, a la `develop`, los cambios se podran
ver reflejados en nuestro servidor interno `islasgeci.org`


## 3. Instalación
En este punto podemos hacer una instalacion automatica la cual nos dara una
configuración ya establecida por el equipo de Ciencia de Datos:

### 3.1 Instalación automática
Nota: Es necesario que el puerto 80 esté libre.
``` bash
make autoinstall
```

### 3.2 Instalación manual (opcional)
Esta instrucción nos permite configurar wordpress con diferentes parametros. En
este caso no utilizaremos esta instrucción ya que los parámetros que necesitamos
están declarados en `example.env` y configurados en la instalación automática.
``` bash
sudo make install
```

### 3.3 Limpieza manual (utilidad)
En caso de que necesitemos hacer una instalación de wordpress otra vez,
necesitamos limpiar los archivos generados por la instalación anterior. en este
caso podemos usar la siguiente instrucción:

``` bash
sudo make clean
```

## 4. Prueba la nueva instalación del blog de BFAL
Visita la dirección <http://localhost:80> para probar la nueva instalación.

Las credenciales por defecto son:
- `Usuario: wordpress`
- `Contraseña: wordpress`

# Instalación en servidor interno islasgeci.org
Para instalar todo (contenedores, carga de base de datos, tema y ficheros
multimedia) en nuestro servidor interno debemos ejecutar la siguiente línea en
nuestra computadora local:

`make internal_deploy`

**_NOTA:_** Lo primero que hace esta instrucción es limpiar los ficheros,
directorios y contenedores que pudieran estar en el servidor, para a
continuación levantar todo de nuevo, con los últimos cambios que pudieran haber.

Si se desea únicamente limpiar  los ficheros, directorios y contenedores,
debemos ejecutar la siguiente instrucción:

`make internal_clean`

Si todo sale bien, al final de la instalación aparecerá el siguiente texto en la
consola:

`¡La instalación fue un éxito!`

De lo contrario hubo algún problema con la instalación.

## Prueba la nueva instalación del blog BFAL en el servidor interno
Visita la dirección <http://islasgeci.org/> para probar la nueva instalación del
blog.



## Material extra
- [Revisión local del blog de BFAL - video tutorial de
  explicación](https://drive.google.com/file/d/1p30INPRKsPiXD5c1kQZ51i3HfJLLdIgV/view?usp=sharing)
- [_Pull
  request_](https://github.com/kassambara/wordpress-docker-compose/pull/4)

Con 💖, el equipo de Ciencia de Datos de GECI 😊
